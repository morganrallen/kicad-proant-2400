# ProAnt 2400 SMD '3D' Antenna

[Datasheet](http://www.saelig.com/supplier/proant/OnBoard%20SMD%202400%20Application%20note%20v1_0_Saelig.pdf)

|       Symbol          |         Footprint           |
|-----------------------|-----------------------------|
| ![symbol](symbol.png) | ![footprint](footprint.png) | 

# Incorporating into your project
It's assumed this will be included as a git submodule in your project.
```
git submodule add https://gitlab.com/morganrallen/kicad-proant-2400.git lib/ProAnt_2400
```

# 3D model
Due to short-comings in the way KiCAD handles relative paths for footprints, if the repository is
not checked out to `lib/ProAnt_2400` the model will not work without further action.

If for some reason you cannot checkout to that location, you will need to copy `ProAnt.pretty/ProAnt.step`
to your KiCAD installation 3d package directory.

* Linux: /usr/share/kicad/modules/packages3d/

![3d model](3d_preview.png)
